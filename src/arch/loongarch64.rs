use core::{mem, slice};
use core::ops::{Deref, DerefMut};

use super::error::{Error, Result};

pub const PAGE_SIZE: usize = 4096;

macro_rules! syscall {
  ($($name:ident($a:ident, $($b:ident, $($c:ident, $($d:ident, $($e:ident, $($f:ident, )?)?)?)?)?);)+) => {
      $(
          pub unsafe fn $name($a: usize, $($b: usize, $($c: usize, $($d: usize, $($e: usize, $($f: usize)?)?)?)?)?) -> Result<usize> {
              let ret: usize;

              core::arch::asm!(
                  "syscall 0",
                  in("$a7") $a,
                  $(
                      in("$a0") $b,
                      $(
                          in("$a1") $c,
                          $(
                              in("$a2") $d,
                              $(
                                  in("$a3") $e,
                                  $(
                                      in("$a4") $f,
                                  )?
                              )?
                          )?
                      )?
                  )?
                  lateout("$a0") ret,
                  options(nostack),
              );

              Error::demux(ret)
          }
      )+
  };
}

syscall! {
  syscall0(a,);
  syscall1(a, b,);
  syscall2(a, b, c,);
  syscall3(a, b, c, d,);
  syscall4(a, b, c, d, e,);
  syscall5(a, b, c, d, e, f,);
}

#[derive(Copy, Clone, Debug, Default)]
#[repr(C)]
pub struct IntRegisters {
  pub r31: u64,
  pub r30: u64,
  pub r29: u64,
  pub r28: u64,
  pub r27: u64,
  pub r26: u64,
  pub r25: u64,
  pub r24: u64,
  pub r23: u64,
  pub r22: u64,
  // r21 is preserved
  // pub r21: u64,
  pub r20: u64,
  pub r19: u64,
  pub r18: u64,
  pub r17: u64,
  pub r16: u64,
  pub r15: u64,
  pub r14: u64,
  pub r13: u64,
  pub r12: u64,
  pub r11: u64,
  pub r10: u64,
  pub r9: u64,
  pub r8: u64,
  pub r7: u64,
  pub r6: u64,
  pub r5: u64,
  pub r4: u64,
  pub r3: u64,
  // r2 is tp
  // pub r2: u64,
  pub r1: u64,
  pub r0: u64,
}

impl Deref for IntRegisters {
  type Target = [u8];
  fn deref(&self) -> &[u8] {
      unsafe {
          slice::from_raw_parts(self as *const IntRegisters as *const u8, mem::size_of::<IntRegisters>())
      }
  }
}

impl DerefMut for IntRegisters {
  fn deref_mut(&mut self) -> &mut [u8] {
      unsafe {
          slice::from_raw_parts_mut(self as *mut IntRegisters as *mut u8, mem::size_of::<IntRegisters>())
      }
  }
}

#[derive(Clone, Copy, Debug, Default)]
#[repr(packed)]
pub struct FloatRegisters {
  pub f30: u64,
  pub f29: u64,
  pub f28: u64,
  pub f27: u64,
  pub f26: u64,
  pub f25: u64,
  pub f24: u64,
  pub f23: u64,
  pub f22: u64,
  pub f21: u64,
  pub f20: u64,
  pub f19: u64,
  pub f18: u64,
  pub f17: u64,
  pub f16: u64,
  pub f15: u64,
  pub f14: u64,
  pub f13: u64,
  pub f12: u64,
  pub f11: u64,
  pub f10: u64,
  pub f9: u64,
  pub f8: u64,
  pub f7: u64,
  pub f6: u64,
  pub f5: u64,
  pub f4: u64,
  pub f3: u64,
  pub f2: u64,
  pub f1: u64,
  pub f0: u64,
}

impl Deref for FloatRegisters {
  type Target = [u8];
  fn deref(&self) -> &[u8] {
      unsafe {
          slice::from_raw_parts(self as *const FloatRegisters as *const u8, mem::size_of::<FloatRegisters>())
      }
  }
}

impl DerefMut for FloatRegisters {
  fn deref_mut(&mut self) -> &mut [u8] {
      unsafe {
          slice::from_raw_parts_mut(self as *mut FloatRegisters as *mut u8, mem::size_of::<FloatRegisters>())
      }
  }
}

#[derive(Clone, Copy, Debug, Default)]
#[repr(packed)]
pub struct EnvRegisters {
  pub r21: u64,
  pub tp: u64,
}

impl Deref for EnvRegisters {
  type Target = [u8];
  fn deref(&self) -> &[u8] {
      unsafe {
          slice::from_raw_parts(self as *const EnvRegisters as *const u8, mem::size_of::<EnvRegisters>())
      }
  }
}

impl DerefMut for EnvRegisters {
  fn deref_mut(&mut self) -> &mut [u8] {
      unsafe {
          slice::from_raw_parts_mut(self as *mut EnvRegisters as *mut u8, mem::size_of::<EnvRegisters>())
      }
  }
}
